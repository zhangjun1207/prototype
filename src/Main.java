public class Main {

    public static void main(String[] args) {
        Address address = new Address("中国", "江苏", "南京");
        Customer c1 = new Customer("Tom", address);
        Customer c2 = c1.clone();
        //浅克隆会更改原对象的值
        c2.getAddress().setCity("南通");
        System.out.println(c1.toString());
        System.out.println(c2.toString());
    }
}
