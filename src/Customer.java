
public class Customer implements Cloneable {
    private String name;
    private Address address;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Customer(String name, Address address) {
        super();
        this.name = name;
        this.address = address;
    }

    @Override
    public String toString() {
        return "Customer [address=" + address + ", name=" + name + "]";
    }

    public Customer clone() {
        Customer clone = null;
        try {
            clone = (Customer) super.clone();
//            clone.address = (Address) address.clone();
            clone.address = (Address) address.deepClone();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return clone;
    }
}
